const express = require("express");
const app = express();
const port = 8000;
const userRouter = require("./app/router/userRouter")
const contactRouter = require("./app/router/contactRouter")
const bodyParser = require("body-parser")
const url = bodyParser.urlencoded({extended: false})
const mongoose = require("mongoose");
      mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Vaccine")
              .then(() => console.log("Successful"))
              .catch((err) => console.log(err))
app.set("view engine", "ejs")
app.use(express.json());
app.use(express.static("views"));

app.use((req, res, next) => {
  console.log("Current date: ", new Date());
  next()
})
app.get("/", (req, res) => {
  console.log(__dirname);
  res.render("index")
 // res.sendFile(__dirname + "/views/index.html")
})

app.use("/", url, userRouter)
app.use("/", url, contactRouter)

app.listen(port, () => {
  console.log("app listening on", port)
})