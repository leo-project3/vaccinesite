# Vccine.ng Page

## 📄 Description
### Website for client to post data from form to database

## ✨ Functions
+ Display ordered Pizza on Admin Table
![Vaccination.ng Home Page](./views/img/display.png)

+ Create new user
![CreateUser](./views/img/Create%20user.png)

+ Get User by Name and Phone
![GetUser](./views/img/getUserByNameandPhone.png)

+ Create Contact Email
![CreateContactEmal](./views/img/createContactEmail.png)

# Another tech
  CRUD to all API USER and CONTACT THROUGH POSTMAN
## 💻 Techs

+ Front End:
   1. [Bootstrap](https://getbootstrap.com/docs/5.3/getting-started/introduction/)
   2. Vanila Javascript
   3. Jquery
   4. Ajax
   5. Local Storage
   6. JSON

+ Back End:
  1. ExpressJs 
  2. DataBase: MongoDb

## Attention !!!
#### Update User only allow to change the status, not allowed to change name and phone number.
#### Responsive website for laptop and tablet. Not available for mobile. Waiting for update