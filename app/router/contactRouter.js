const express = require("express")
const router = express.Router();

const {
  getContactMiddleware,
  getContactByIdMiddleware,
  createContactMiddleware,
  updateContactMiddleware,
  deleteContactMiddleware
} = require("../middleware/contactMiddleware")
const
 { createContact,
  getAllContact,
  getContactById,
  updateContact,
  deleteContact } = require("../controller/contactController")

router.post("/admin/contact", createContactMiddleware, createContact)
router.get("/admin/contact", getContactMiddleware, getAllContact)
router.get("/admin/contact/:id", getContactByIdMiddleware, getContactById)
router.put("/admin/contact/:id", updateContactMiddleware, updateContact)
router.delete("/admin/contact/:id", deleteContactMiddleware, deleteContact)
module.exports = router