const express = require("express");
const router = express.Router();

const {
  getUserMiddleware,
  getUserByIdMiddleware,
  createUserMiddleware,
  updateUserMiddleware,
  deleteUserMiddleware
} = require("../middleware/userMiddleware")

const {
  createNewUser,
  getUser,
  getUserById,
  updateUser,
  deleteUserById
} = require("../controller/userController")

router.post("/admin/user", createUserMiddleware, createNewUser)
router.get("/admin/user", getUserMiddleware, getUser)
router.get("/admin/user/:id", getUserByIdMiddleware, getUserById)
router.put("/admin/user/:id", updateUserMiddleware, updateUser)
router.delete("/admin/user/:id", deleteUserMiddleware, deleteUserById)
module.exports = router