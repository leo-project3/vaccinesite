const express = require("express");
const mongoose = require("mongoose")
const userModel = require("../model/userModel")

const router = express.Router();


const createNewUser = (req, res) => {
  const fullName = req.body.fullName;
  const phone = req.body.phone
  const regexPhoneNumber = /(84|0[3|5|7|8|9])+([0-9]{8})\b/g;
  if(!phone.match(regexPhoneNumber)){
    return res.status(404).json({
      message: "Phone number is not valid"
    })
  }
  var newUser = new userModel({
    _id: new mongoose.Types.ObjectId,
    fullName,
    phone
  })
  userModel.create(newUser)
           .then((data) => {
            res.status(201).json({
              message: "Create user successful",
              user: data
            })
           })
           .catch((error) => {
            res.status(500).json({
              message:"error" + error.message
            })
           })
}

const getUser = async (req, res) => { 
 console.log(req.query)
 const fullName = req.query.fullName
 const phone = req.query.phone

  
  try {
    if(fullName == "" && phone == ""){
      return res.status(400).json({
        message: "fullname or phone cant blank"
      })
     }
    const result = await userModel.find({fullName}).find({phone});
     if(result == ""){
      return res.status(400).json({
        message: "No user found"
      })
     }
    res.status(200).json({
      message: "Get user successfully",
      users: result
    })
  } catch (error) {
    res.status(500).json({
      message:"error" + error.message
    })
  }
}

const getUserById = async (req, res) => {
  const id = req.params.id
  if(!mongoose.Types.ObjectId.isValid(id)){
    return res.status(404).json({
      message: "Your id is not valid"
    })
  }
  try {
    const result = await userModel.findById(id);
    if(result) {
      res.status(200).json({
        message: `Get user by id ${id} successfully`,
        user: result
      })
    } else {
      res.status(400).json({
        message: "Id is not found"
      })
    }
  } catch (error) {
    res.status(500).json({
      message:"error" + error.message
    })
  }
}

const updateUser = async (req, res) => {
  const id = req.params.id;
  const status = req.body.status
  if(!mongoose.Types.ObjectId.isValid(id)){
    return res.status(404).json({
      message: "Your id is not valid"
    })
  }
  
  if(status != "Level 1" && status != "Level 2" && status != "Level 3"){
    return res.status(400).json({
      message: `Your status should be Level 1 or Level 2 or Level 3`
    })
  }
  try {
    var updateUser = {};
    if(status) {
      updateUser.status = status;
    }
    const result = await userModel.findByIdAndUpdate(id, updateUser);
    if(result) {
      res.status(200).json({
        message: `Update user by id ${id} successfully`,
        user: result
      })
    } else {
      res.status(400).json({
        message: "Id is not found"
      })
    }
  } catch (error) {
    res.status(500).json({
      message:"error" + error.message
    })
  }
}

const deleteUserById = async (req, res) => {
  const id = req.params.id
  if(!mongoose.Types.ObjectId.isValid(id)){
    return res.status(404).json({
      message: "Your id is not valid"
    })
  }
  try {
    const result = await userModel.findByIdAndDelete(id);
    res.status(200).json({
      message: `Delete user ${id} successfully`,
      user: result
    })
  } catch (error) {
    res.status(500).json({
      message:"error" + error.message
    })
  }
}
module.exports = {
  createNewUser,
  getUser,
  getUserById,
  updateUser,
  deleteUserById
}
