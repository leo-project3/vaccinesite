const contactModel = require("../model/contactModel")
const mongoose = require("mongoose")

const createContact = (req, res) => {
  const email = req.body.email
  const emailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g
  if(!email.match(emailRegex)){
    res.status(400).json({
      message:"Your email is not valid"
    })
  }
  var newContact = new contactModel({
    _id: new mongoose.Types.ObjectId,
    email
  })
  contactModel.create(newContact)
           .then((data) => {
            res.status(201).json({
              message: "Create contact successful",
              contact: data
            })
           })
           .catch((error) => {
            res.status(500).json({
              message:"error" + error.message
            })
           })
}

const getAllContact = async (req, res) => {
  try {
    const result = await contactModel.find();
    return res.status(200).json({
      message: "Get all contact successfully",
      contacts: result
    })
  } catch (error) {
    return res.status(500).json({
      message:"Error" + error.message
    })
  }
}

const getContactById = async (req, res) => {
  const id = req.params.id;
  if(!mongoose.Types.ObjectId.isValid(id)){
    return res.status(400).json({
      message: "Your id is not valid"
    })
  }
  try {
    const result = await contactModel.findById(id)
    if(result){
      res.status(200).json({
        message: `Get contact by id ${id}`,
        contact: result
      })
    } else {
      res.status(400).json({
        message: "Id is not found"
      })
    }
  } catch (error) {
    return res.status(500).json({
      message:"Error" + error.message
    })
  }
}

const updateContact = async (req, res) => {
  const id = req.params.id;
  if(!mongoose.Types.ObjectId.isValid(id)){
    return res.status(400).json({
      message: "Your id is not valid"
    })
  }
  const email = req.body.email
  const emailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g
  if(!email.match(emailRegex)){
    return res.status(404).json({
       message:"Your email is not valid"
    })
  }
  try {
    var updateContact = {}
    if(email){
      updateContact.email = email
    }
    const result = await contactModel.findByIdAndUpdate(id, updateContact)
    if(result){
      res.status(200).json({
        message: `Update contact by id ${id}`,
        contact: result
      })
    } else {
      res.status(400).json({
        message: "Id is not found"
      })
    }
  } catch (error) {
    return res.status(500).json({
      message:"Error" + error.message
    })
  }
}

const deleteContact = async (req, res) => {
  const id = req.params.id
  if(!mongoose.Types.ObjectId.isValid(id)){
    return res.status(404).json({
      message: "Your id is not valid"
    })
  }
  try {
    const result = await contactModel.findByIdAndDelete(id);
    res.status(200).json({
      message: `Delete contact ${id} successfully`,
      user: result
    })
  } catch (error) {
    res.status(500).json({
      message:"error" + error.message
    })
  }
}
 module.exports = {
  createContact,
  getAllContact,
  getContactById,
  updateContact,
  deleteContact
}