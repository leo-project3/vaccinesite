const getUserMiddleware = (req, res, next) => {
  console.log("Get user");
  next()
}
const getUserByIdMiddleware = (req, res, next) => {
  console.log("Get user by Id");
  next()
}
const createUserMiddleware = (req, res, next) => {
  console.log("Create user");
  next()
}
const updateUserMiddleware = (req, res, next) => {
  console.log("Update user");
  next()
}
const deleteUserMiddleware = (req, res, next) => {
  console.log("Delete user");
  next()
}
module.exports = {
  getUserMiddleware,
  getUserByIdMiddleware,
  createUserMiddleware,
  updateUserMiddleware,
  deleteUserMiddleware
}
