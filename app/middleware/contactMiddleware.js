const getContactMiddleware = (req, res, next) => {
  console.log("Get Contact");
  next()
}
const getContactByIdMiddleware = (req, res, next) => {
  console.log("Get Contact by Id");
  next()
}
const createContactMiddleware = (req, res, next) => {
  console.log("Create Contact");
  next()
}
const updateContactMiddleware = (req, res, next) => {
  console.log("Update Contact");
  next()
}
const deleteContactMiddleware = (req, res, next) => {
  console.log("Delete Contact");
  next()
}
module.exports = {
  getContactMiddleware,
  getContactByIdMiddleware,
  createContactMiddleware,
  updateContactMiddleware,
  deleteContactMiddleware
}
